package es.mdef.divindes.microservicios.app.usuarios.services;

import java.util.Optional;

import es.mdef.divindes.microservicios.app.usuarios.models.entity.Usuario;

public interface UsuarioService {
	
	public Iterable<Usuario> findAll();
	
	public Optional<Usuario> findById(Long id);
	
	public Usuario save(Usuario usuario);
	
	public void deleteById(Long id);
	
}
