package es.mdef.divindes.microservicios.app.usuarios.modesl.repository;

import org.springframework.data.repository.CrudRepository;

import es.mdef.divindes.microservicios.app.usuarios.models.entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

}
